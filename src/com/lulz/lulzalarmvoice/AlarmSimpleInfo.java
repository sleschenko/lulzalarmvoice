package com.lulz.lulzalarmvoice;

import java.util.Date;

public class AlarmSimpleInfo{
	private int id;
	private Date dt;
	private String text; 
	public AlarmSimpleInfo(int id, Date dt, String text) {
		super();
		this.id = id;
		this.dt = dt;
		this.text = text;
	}
	public int getId() {
		return id;
	}
	public Date getDt() {
		return dt;
	}
	public String getText() {
		return text;
	}
	
	@Override
	public String toString(){
		return String.format("Alarm[%s: '%s' - '%s']", id, dt, text);
	}
}

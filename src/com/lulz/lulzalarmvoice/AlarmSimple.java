package com.lulz.lulzalarmvoice;

import java.util.ArrayList;
import java.util.List;

public class AlarmSimple {

	private List<AlarmSimpleInfo> alarmList;

	public AlarmSimple() {
		super();
		alarmList = new ArrayList<AlarmSimpleInfo>();
	}
	
	public void addAlarm(AlarmSimpleInfo asi){
		alarmList.add(asi);
	}
	
	public void setAlarms(ArrayList<AlarmSimpleInfo> alarms){
		alarmList = alarms;
	}

	public ArrayList<String> getAlarmTextList(){
		ArrayList<String> textList = new ArrayList<String>();
		for(AlarmSimpleInfo asi : alarmList){
			textList.add(asi.getText());
		}
		return textList;
	}
}

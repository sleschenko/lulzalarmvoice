package com.lulz.lulzalarmvoice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lulz.lulzalarmvoice.MainActivity;

import android.util.Log;

public class CommandProcessor {

	public static final String CMD_GET_ALARM_SIMPLE = "get_alarm_simple";

	private static final String SECTION_ACCOUNT = "login";
	private static final String SECTION_RESPONSE = "res";
	private static final String SECTION_DATA = "data";

	private static final String JSON_ERROR = "Can`t convert string to JSON object: ";
	
	public static final String RESPONSE_OK = "ok";
	public static final String LOGIN_OK = "ok";

	private JSONObject createJSONObject(String jsonString) {
		JSONObject jsonObject = null;
		try {
			jsonObject = new JSONObject(jsonString);
		} catch (JSONException e) {
			Log.e(MainActivity.LOG_TAG, JSON_ERROR + jsonString);
		}
		return jsonObject;
	}

	private boolean validateResponseResult(String response) {
		if (response.equals(RESPONSE_OK)) {
			return true;
		}
		return false;
	}

	private boolean validateLoginResult(String response) {
		if (response.equals(LOGIN_OK)) {
			return true;
		}
		return false;
	}

	private boolean validateGeneralSections(JSONObject jsonObject) {
		if (jsonObject.has(SECTION_ACCOUNT)) {
			if (jsonObject.has(SECTION_RESPONSE)) {
				if (jsonObject.has(SECTION_DATA)) {
					return true;
				}
			}
		}
		return false;
	}

	public String getResponseData(String jsonString) {
		JSONObject jsonObject = createJSONObject(jsonString);
		if (jsonObject != null) {
			if (validateGeneralSections(jsonObject)) {
				try {
					String loginData = jsonObject.getString(SECTION_ACCOUNT);
					String responseData = jsonObject
							.getString(SECTION_RESPONSE);
					String paramData = jsonObject.getString(SECTION_DATA);

					if (validateResponseResult(responseData)
							&& validateLoginResult(loginData)) {
						return paramData;
					}
				} catch (JSONException e) {
					Log.e(MainActivity.LOG_TAG,
							"process json fial: " + e.getMessage());
				}
			}
		}
		return null;
	}

	public ArrayList<AlarmSimpleInfo> procGetAlarmSimple(String params) {
		JSONObject json = createJSONObject(params);
		ArrayList<AlarmSimpleInfo> alarmList = new ArrayList<AlarmSimpleInfo>();
		if (json != null) {
			if (json.has("alarms")) {
				try {
					String alarmsString = json.getString("alarms");
					JSONArray alarms = new JSONArray(alarmsString);
					
					for (int i = 0; i < alarms.length(); i++) {
						JSONObject alarm = alarms.getJSONObject(i);

						int id = alarm.optInt("id", 0);
						String text = alarm.optString("text", "");
						String dtStr = alarm.optString("dt");

						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0", Locale.US);
						// format.applyPattern("yyyy-MM-dd HH:mm:ss.0");
						Date dt = format.parse(dtStr);

						AlarmSimpleInfo as = new AlarmSimpleInfo(id, dt, text);
						alarmList.add(as);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (ParseException e) {

					e.printStackTrace();
				}
			}
		}
		return alarmList;
	}
}

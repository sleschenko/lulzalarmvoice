package com.lulz.lulzalarmvoice;

import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class MainActivity extends Activity {

	public static final String LOG_TAG = "LAlarmVoice";

	public static EditText teLogin;
	public static EditText tePwd;
	private Button bSendRequest;
	private Button bTalk;
	private static TextView tvLog;
	private ListView listAlarms;
	private ListView listEngines;
	private RadioGroup radioLng;
	
	public AlarmSimple as;
	public CommandProcessor cp;
	private SpeechManager sm;
	private AlarmManagerBroadcastReceiver am;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		bSendRequest = (Button) findViewById(R.id.ButtonSendRequest);
		bTalk = (Button) findViewById(R.id.ButtonTalk);
		tvLog = (TextView) findViewById(R.id.textViewLog);
		teLogin = (EditText) findViewById(R.id.EditTextLogin);
		tePwd = (EditText) findViewById(R.id.EditTextPwd);
		listAlarms = (ListView) findViewById(R.id.listAlarms);
		listEngines = (ListView) findViewById(R.id.listEngines);
		radioLng = (RadioGroup) findViewById(R.id.radioGroupLng);

		sm = new SpeechManager( this );
		as = new AlarmSimple();
		cp = new CommandProcessor();
		am = new AlarmManagerBroadcastReceiver();
		
		makeEnginesList();
        logAdd(sm.getSpeechParam());
		
		bTalk.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v)
			{
			}
		});
		
		listAlarms.setOnItemClickListener(new OnItemClickListener() {
		    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		    {
		        TextView wordView = (TextView)view;
		        String wordChosen = (String) wordView.getText();
		        sm.speak(wordChosen);
		    }
		});

		listEngines.setOnItemClickListener(new OnItemClickListener() {
		    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		    {
		        TextView wordView = (TextView)view;
		        String engineName = (String) wordView.getText();
		        logAdd("try to set engine to: " + engineName);
		        sm.setEngine(engineName);
		    }
		});
		
		radioLng.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.radioRU:
					sm.setLng(1);
					break;
				case R.id.radioUK:
					sm.setLng(2);
					break;
				case R.id.radioUS:
					sm.setLng(0);
					break;
				}
			}
		});
	}

	public static void logAdd(String line){
		CharSequence s = tvLog.getText();
		tvLog.setText(line + "\n" + s);
	}
	
	public void makeAlarmList(){
        listAlarms.setAdapter(new ArrayAdapter<String>(this, R.layout.word, as.getAlarmTextList()));
	}

	private void makeEnginesList(){
        listEngines.setAdapter(new ArrayAdapter<String>(this, R.layout.word, sm.getEngines()));
	}

	@Override
    public void onDestroy() {
		sm.stopSpeechManager();
        super.onDestroy();
    }
 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onClickSendButton(View v){
		connect();
	}
	
	public void onClickSetAlarm(View v){
		Context context = this.getApplicationContext();
        if (am != null) {
            am.SetAlarm(context);
        } else {
            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }	
	}

	public void onClickCancelAlarm(View v){
        Context context = this.getApplicationContext();
        if (am != null) {
            am.CancelAlarm(context);
        } else {
            Toast.makeText(context, "Alarm is null", Toast.LENGTH_SHORT).show();
        }	
	}
	
	public void showSettingsActivity(){
		Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
		intent.putStringArrayListExtra("engines", sm.getEngines());
	    startActivity(intent);	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			showSettingsActivity();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void connect() {
		new ProcessRequest().execute(this);
	}

}

package com.lulz.lulzalarmvoice;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lulz.lulzalarmvoice.MainActivity;
import com.lulz.lulzalarmvoice.WebService;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

public class ProcessRequest extends AsyncTask<Activity, Void, String> {

	public ProcessRequest() {
	}

	private MainActivity activity;
	
	@Override
	protected String doInBackground(Activity... activity) {
		this.activity = (MainActivity) activity[0];
		
		// Instantiate the Web Service Class with he URL of the web service not
		// that you must pass

		// WebService webService = new
		// WebService("http://91.234.34.215:8080/lulzws2/rest/todos");
		WebService webService = new WebService("http://lulzalarm.restart.org.ua/lulz/userdata/dc");
		Map<String, Object> params = new HashMap<String, Object>();

		String log = MainActivity.teLogin.getText().toString();
		String pwd = MainActivity.tePwd.getText().toString();
		log = "bream";
		pwd = "[eq[eq[eq";
		JSONObject json = new JSONObject();
		
		params.put("account", String.format("{login:\"%s\",pwd:\"%s\"}", log, pwd));
		params.put("command", "get_alarm_simple");
		params.put("data", "{id:1}");

		String response = webService.webInvoke("", params);

		try {
			// Parse Response into our object
			// Type collectionType = new TypeToken<List<String>>(){}.getType();
			// List<String> alrt = new Gson().fromJson(response,
			// collectionType);
			//Log.e(MainActivity.LOG_TAG, response);
		} catch (Exception e) {
			Log.e(MainActivity.LOG_TAG, e.getMessage());
		}
		return response;
	}
	   @Override
       protected void onPostExecute(String response) {
		   String data = activity.cp.getResponseData(response);
		   if(data == null) data = "res - fail";

		   activity.as.setAlarms(activity.cp.procGetAlarmSimple(data));
		   
		   activity.makeAlarmList();
	   }


}

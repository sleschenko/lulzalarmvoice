package com.lulz.lulzalarmvoice;

import java.util.ArrayList;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.Activity;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.EngineInfo;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;


@TargetApi(18)
public class SpeechManager implements OnInitListener{

	private static final String NOT_SUPPORTED_LNG_MSG = "This Language is not supported!"; 
	private static final String INIT_FAILED_MSG = "Initilization Failed!";	
	private TextToSpeech tts;
	private Activity ttsContext;
	
	public TextToSpeech getTts() {
		return tts;
	}

	@Deprecated
	public SpeechManager(TextToSpeech tts) {
		super();
		this.tts = tts;
	}

	public SpeechManager(Activity context) {
		super();
		ttsContext = context;
		initTTS(""); 
	}
	
	private void initTTS(String engine){
		if(engine.equals("")){
			tts = new TextToSpeech(ttsContext, this);
		}else{
			tts = new TextToSpeech(ttsContext, this, engine);
		}
	}
	
	private void reInitTTS(String newEngine){
		stopSpeechManager();
		initTTS(newEngine);
	}
	
	public void stopSpeechManager(){
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
	}
	
	public ArrayList<String> getEngines(){
		ArrayList<String> engines = new ArrayList<String>();
		for(EngineInfo ei : tts.getEngines()){
			engines.add(ei.name);
		}
		return engines;
	}
	
	public String getSpeechParam(){
		String lng = "n/a";
		String defLng = "n/a";

		Locale loc = tts.getLanguage();
		if(loc != null){
			lng = loc.getLanguage();
		}
		String defEngine = tts.getDefaultEngine();

		//loc = tts.getDefaultLanguage();
		if(loc != null){
			//defLng = loc.getLanguage();
		}
		
		//tts.getFeatures(locale)
		
		return String.format("lng: %s\n def lng: %s\n def engine: %s", lng, defLng, defEngine);
	}

	public void setEngine(String newEngine){
		reInitTTS(newEngine);
	}
	
	public void setLng(int locale){
		String msg = "try to set locale to: ";
		Locale loc;
		switch (locale) {
		case 1:
			loc = new Locale("ru");
			msg += "ru";
			break;

		case 2:
			loc = Locale.UK;
			msg += "uk";
			break;
		
		default:
			loc = Locale.US;
			msg += "us";
			break;
		}
        int result = tts.setLanguage(loc);
        MainActivity.logAdd(msg);
        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            Log.e(MainActivity.LOG_TAG, NOT_SUPPORTED_LNG_MSG);
            MainActivity.logAdd(NOT_SUPPORTED_LNG_MSG);
        } else {
        	MainActivity.logAdd("ok");
        }
	}
	
	@SuppressWarnings("unused")
	private void testTTS(){
		String defEngine = tts.getDefaultEngine();
		
		String defLanguage = tts.getDefaultLanguage().toString();
		
		Log.e(MainActivity.LOG_TAG, defEngine + " by def. Supported engine:");

		for(EngineInfo ei : tts.getEngines()){
			Log.e(MainActivity.LOG_TAG, ei.name);
			if(ei.name.contains("pico")){
				defEngine = ei.name;
				break;
			}
		}
		Log.e(MainActivity.LOG_TAG, defEngine + " by def. Supported engine:");
	}

	public void speak(String stringToSpeak){
        tts.speak(stringToSpeak, TextToSpeech.QUEUE_FLUSH, null);
	}

	@Override
	public void onInit(int status) {
		 
        if (status == TextToSpeech.SUCCESS) {
        	setLng(0);
        } else {
            Log.e(MainActivity.LOG_TAG, INIT_FAILED_MSG);
        }
	}
	
}





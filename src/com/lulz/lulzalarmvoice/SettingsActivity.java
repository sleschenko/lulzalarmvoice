package com.lulz.lulzalarmvoice;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class SettingsActivity extends Activity{
	
	private ListView listEngines;
	//private RadioGroup rgEngines;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
//		listEngines = (ListView) findViewById(R.id.listEngines);
		//rgEngines = (RadioGroup) findViewById(R.id.radioGroupEngines);
		
		//rgEngines.
		
		
		ArrayList<String> engines = getIntent().getStringArrayListExtra("engines");
		fillEnginesList(engines);
	}
	
	public void fillEnginesList(ArrayList<String> engines){
		listEngines.setAdapter(new ArrayAdapter<String>(this, R.layout.word, engines));
	}
}
